<div style="text-align:right;color:grey">Dernière mise à jour : 11 septembre 2019</div>
[Retour à l'index](index.html)

# Serveur web

Il me faut un serveur sur la framboise. Un truc basic apache + php + MySql.

Le tuto que j'essaye de dérouler : https://raspberry-pi.fr/installer-serveur-web-raspberry-lamp/

Je suis sur mon PC principal accédant au Raspberry via *ssh*.

## Apache

Le serveur web. Le plus simple. 

<code>sudo apt install apache2</code>

Une fois installé, on utilise un navigateur pour vérifier, dans la barre d'adresse on met juste l'adresse *ip* du Raspberry. *It works!*

![1566205562697](assets/1566205562697.png)

## PHP

<code>sudo apt install php php-mbstring</code>

Contrairement à ce qui est indiqué dans le tuto qui doit dater un peu (pourtant mis à jour en juin 2019), on se retrouve avec du PHP 7, pas 5.

## MySql

<code>sudo apt install mysql-server php-mysql</code>

Oui  mais non. 

![1566209011081](assets/1566209011081.png)

> Aucune version du paquet mysql-server n'est disponible...

Utiliser MariaDB au lieu de MySQL? Ok mais quelles différences?

A première vue après quelques recherches je crois comprendre que ce fork de libristes historiques du développement MySQL serait bien, voire mieux que l'original, et 100% compatible. Donc on essaye. Mais comment on administre ses bases, avec *phpMyAdmin*? Il semble que oui.

Installation MariaDB sur Raspbian ([source](https://raspberry-pi.fr/installer-mariadb-raspbian/))

```
sudo apt-get install mariadb-server
```

Pas de soucis.

## phpMyAdmin

Installation de phpMyAdmin ([source](https://www.adsysteme.com/installer-phpmyadmin-pour-utiliser-mariadb-sur-un-navigateur-web/))

```
sudo apt-get install phpmyadmin
```

L'interface de phpMyAdmin devrait être dispo sur http://ipdelaraspberry/phpmyadmin, mais ce n'est pas le cas :cry:. L'erreur n'est pas un problème de droits, mais un 404 (not found).

Après recherches ([solution trouvée ici](https://askubuntu.com/questions/387062/how-to-solve-the-phpmyadmin-not-found-issue-after-upgrading-php-and-apache)), j'ai ajouté la ligne suivante à la fin du fichier */etc/apache2/apache2.conf*

`Include /etc/phpmyadmin/apache.conf`

Mon *phpMyAdmin* semble fonctionner mais impossible de me connecter. Pas trouvé le mot de passe par défaut, impossible de me loguer avec le compte root.

![image-20190823234046600](assets/image-20190823234046600.png)

J'arrive à lancer un <code>sudo mysql</code>, j'attaque directement la base ***mysql*** et je crée un utilisateur à qui je donne tous les droits

`MariaDB [(none)]> use mysql;`

`MariaDB [mysql]> create user 'jcfrog'@'localhost' identified by 'monmotdepasse';`

`MariaDB [mysql]> grant all privileges on *.* to jcfrog@localhost;`

Et ça marche. Happy! Je me connecte avec ce compte, je crée une première base *jcdevdb* and roll my hen !

![image-20190823234836215](assets/image-20190823234836215.png)

Note : je constaterai après coup en fouillant la base que le user pour se connecter était *phpmyadmin* ...

Il semble que ma framboise soit opérationnelle s'agissant du serveur Web/PHP/MySQL. :thumbsup:

![Pharrell Williams - Happy.jpg](assets/220px-Pharrell_Williams_-_Happy.jpg)

Edit 11 septembre 2019 : l'utilisation de phpMyAdmin s'avère enrayée par un chti bug qui génère des messages d'erreur tout rose 

```
Warning in ./libraries/sql.lib.php#613
count(): Parameter must be an array or an object that implements Countable
```

La solution n'est pas jolie jolie: [édition du source de phpmyadmin](https://doc.ubuntu-fr.org/phpmyadmin#incompatibilite_avec_php_72), mais ça marche.

## Tip of the day

J'en ai eu besoin alors je le note ici : **changement du mot de passe *root*** 

<code>sudo su</code> pour passer en super user *root* (on constate que le prompt a changé)

<code>passwd</code> pour demander à changer le mot de passe

<code>exit</code> pour sortir du *su* et revenir à notre user *pi*

![1566405511323](assets\1566405511323.png)

