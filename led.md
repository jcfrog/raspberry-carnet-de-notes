<div style="text-align:right;color:grey">Dernière mise à jour : 29 août 2019</div>
[Retour à l'index](index.html)

# Raspberry : allumer une LED

![1567068264871](assets/1567068264871.png)

Me voilà heureux possesseur d'un petit kit d’électronicien en herbe, et mon vieux Raspberry 2 avec sa [Raspbian](https://www.raspberrypi.org/downloads/raspbian/) est en pleine forme. Comme je suis du genre à me lancer des défis de malade, je n'hésite pas à me jeter dans le vide, un challenge de winner qui n'en veut  : allumer une led ! :muscle: :rotating_light:

Oui je sais, c'est prétentieux. Mais qu'importe la modestie, pourvue qu'on ait l'ivresse.

Avant toute chose, il me semble raisonnable de faire un peu l'inventaire de mon kit. [J'ai acheté ça](https://www.amazon.fr/gp/product/B01IH0IKB0/) sur Amazon. Honte sur moi. Comme dirait Zaz, "*je fais parfois des trucs que je trouve pas bien, mais maintenant je les fais en conscience*". ([source](https://www.franceinter.fr/embed/player/aod/87d85baf-b5ea-48e1-b1b5-e83013b0778c)) Ça rigole pas ici, y'a du level niveau références philosophico-littéraires.

Bon faut que je fasse des flèches et des titres avec [Krita](https://krita.org/fr/). Tant mieux, ça fait partie de mes logiciels a prendre en main depuis ma transhumance linuxienne. Je t'aurais fait ça en 2 coups de cuillère à pot de nutella ( en conscience! ) avec mon ancien environnement mais là, je sens que je vais ramer.

(raming...)

Bon finalement Krita n'est pas l'idéal, on va utiliser un bon vieux [Libre Office Draw](https://fr.libreoffice.org/discover/draw/)![1567071111294](assets/1567071111294.png)

![](assets/kitmicrojc-1567150606218.jpg)

Oui il en manque, me souviens pas de tout ce que m'a expliqué Rémy mais y'a l'essentiel pour notre défi de warrior.

## Quelques détails du kit électronique

**Nom** : *Kuman K66 Electroniques Fans Bundle Kit*. Celui que j'ai commandé sur Amazon [est disponible ici](https://www.amazon.fr/gp/product/B01IH0IKB0/) au moment où j'écris ces lignes (août 2019).

J'apprends du vocabulaire et quelques notions de base.

### Breadboard

J'ai un petit breadboard (planche à pain, ou plus gauloisement planche de test) avec plein de trous pour brancher des trucs.

La chose à savoir c'est que les trous des demi-colonnes de chaque côté sont connectés. Par ailleurs les 2 grandes rangées qui bordent sur la longueur sont également connectées et servent en général à distribuer alim et masse sur la longueur.

Dans l'exemple ci dessous on voit que f-30 et h-30 sont connectés car les f-g-h-i-j de chaque rangée sont connectés. De même avec a-b-c-d-e de l'autre côté. On voit également sur le côté les lignes + et - qui partent le long de la planche.

![1567148042727](assets/1567148042727.png)

Ma planche est petite, 30 rangées, ce qui donne 30 x ( + / - / a b c d e / f g h i j / + / -) = 420 trous.

Pour être bien clair dans les connexions physiques de la planche et ne pas rater l'occasion de pratiquer un nouveau logiciel, je t'ai fait un petit dessin sous [DrawIO desktop](https://about.draw.io/integrations/).



![1567149626993](assets/1567149626993.png)

### Câbles

Pour relier les [GPIO](https://fr.wikipedia.org/wiki/General_Purpose_Input/Output) (pins verticales) du Raspberry au breadboard (trous) nous utilisons des *câbles Dupont mâle/femelle*. Je fais confiance à ton intelligence et ton expérience de la vie pour trouver ce qui rentre dans quoi. 

![1567001047336](assets/1567001047336.png)

Pour faire des ponts entre différentes sections du breadboard, nous avons des fils mâle/mâle que l'ami Shakespeare appelle *Jumper Wire*.

![1567001549790](assets/1567001549790.png)

### LEDs 🚥

Que la lumière soit ! Oui mais de quelle couleur ? Nous disposons de leds rouge,  vertes, bleues (Rémy me signale que les bleues c'est pas bon pour les yeux), des jaunes, des blanches, et même une RGB.

### Résistances

Le premier truc que tu lis partout dès que tu t'intéresses aux LEDs c'est "on ne branche jamais une LED directement, il faut la protéger avec un résistance ! ". Si tu veux des détails, [une vidéo d'électronicien](https://www.youtube.com/watch?v=pO4XxRYxgcE) explique pourquoi.

Pour ce qui est de piloter l'allumage d'une diode, très vite on trouve des tutos pour cela. [Celui-ci](https://raspberry-pi.fr/led-raspberry-pi/) me semble parfait. Je ne te fais donc pas perdre plus de temps avec ma prose pour l'aspect technique, tout est magnifiquement expliqué dedans.

Au final j'ai donc comme indiqué :

* connecté avec les câbles Dupont une masse et un GPIO qu'on commandera par logiciel pour envoyer le jus, le 7 en l'occurrence.
* mis une résistance. Le calcul de la valeur de la résistance est expliqué partout, mais moi j'ai la chance d'avoir un Rémy sous le coude : "LED rouge? tu prends une 100 Ω." Merci Rémy 😎
* ajouté la LED, dans le bon sens !
* écrit le petit script python qui fonctionne en switch comme indiqué. 
* brillamment adapté le code pour faire 3 scripts ultra sophistiqués permettant de : 
  * allumer
  * éteindre 
  * faire clignoter

Quelle aventure ! 

Mon montage : le fil noir est connecté au pin 6 du Raspberry (masse) et le blanc au GPIO 7.

![1567152515115](assets/1567152515115.png)

Un autre logiciel sur lequel je dois me former : le montage vidéo avec [Kdenlive](https://kdenlive.org/fr/). Ne ratons pas l'occasion de fêter dignement cet évènement majeur : l'époustouflante vidéo produite [est en ligne ici](https://aperi.tube/videos/watch/8dcaebde-b3c5-476e-8275-f3735e5986bc), sur une instance [Peertube](https://fr.wikipedia.org/wiki/PeerTube) bien entendu.

![](assets/kdenlive-1567152446948.png)

### Scripts

Les 3 scripts adaptés du tuto de raspberry.fr.

#### led-on.py

```python
#!/usr/bin/env python3
#-- coding: utf-8 --

import RPi.GPIO as GPIO #Importe la bibliothèque pour contrôler les GPIOs

GPIO.setmode(GPIO.BOARD) #Définit le mode de numérotation (Board)
GPIO.setwarnings(False) #On désactive les messages d'alerte

LED = 7 #Définit le numéro du port GPIO qui alimente la led

GPIO.setup(LED, GPIO.OUT) #Active le contrôle du GPIO

GPIO.output(LED, GPIO.HIGH) #On l'allume
```

#### led-off.py

```python
#!/usr/bin/env python3
#-- coding: utf-8 --

import RPi.GPIO as GPIO #Importe la bibliothèque pour contrôler les GPIOs

GPIO.setmode(GPIO.BOARD) #Définit le mode de numérotation (Board)
GPIO.setwarnings(False) #On désactive les messages d'alerte

LED = 7 #Définit le numéro du port GPIO qui alimente la led

GPIO.setup(LED, GPIO.OUT) #Active le contrôle du GPIO

GPIO.output(LED, GPIO.LOW) #On l’éteint
```

### led-clignote.py

Attention là on atteint des sommets.

```python
#!/usr/bin/env python3
#-- coding: utf-8 --

import RPi.GPIO as GPIO #Importe la bibliothèque pour contrôler les GPIOs
import time

GPIO.setmode(GPIO.BOARD) #Définit le mode de numérotation (Board)
GPIO.setwarnings(False) #On désactive les messages d'alerte

LED = 7 #Définit le numéro du port GPIO qui alimente la led

GPIO.setup(LED, GPIO.OUT) #Active le contrôle du GPIO

while True:
    state = GPIO.input(LED) #Lit l'état actuel du GPIO, vrai si allumé, faux si éteint

    if state : #Si GPIO allumé
        GPIO.output(LED, GPIO.LOW) #On l’éteint
    else : #Sinon
        GPIO.output(LED, GPIO.HIGH) #On l'allume
    time.sleep(.2)
```

Les maîtres Jedi de l’algorithmie ne manqueront pas de le noter, ce <code>while True:</code> n'est pas glorieux, il faudra faire Ctrl+C pour stopper ce maelström de photons clignotants.