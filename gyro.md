<div style="text-align:right;color:grey">Dernière mise à jour : 27 août 2019</div>
[Retour à l'index](index.html)

# Raspberry : branchement gyroscope

L'idée est de connecter et utiliser un gyroscope via mon vieux Raspberry 2. Pas d'application visée, juste de l'apprentissage.

Totalement noob, j'ai acheté un peu de matériel de base pour pouvoir prétendre à cet exploit. Dans ce kit je vais juste utiliser le *[breadboard](https://fr.wikipedia.org/wiki/Platine_d%27exp%C3%A9rimentation)* et les *fils*. 

![1566122871205](assets/1566122871205.png)

## Composant MPU 9250/6500

Spécification du composant : [format PDF](https://www.invensense.com/wp-content/uploads/2015/02/PS-MPU-9250A-01-v1.1.pdf)

Adresses registres et description : [format PDF](https://www.invensense.com/wp-content/uploads/2015/02/MPU-9250-Register-Map.pdf)

![1566060728248](assets/1566060728248.png)

### Connecter le gyroscope

![https://pi4j.com/1.2/images/j8header-2b-large.png](assets/j8header-2b-large.png)



Le composant  ***MPU 9250/6500*** est un gyroscope utilisant le bus ***[i2c](https://fr.wikipedia.org/wiki/I2C)***, il faut donc installer ce qu'il faut :

<code> sudo apt-get install python-smbus i2c-tools</code>

Ensuite il faut démarrer l'interface ***i2c*** via <code>sudo raspi-config</code> menu *interfaces*

Niveau câblage, 4 branchements Raspberry / MPU :  

|     Raspberry     | Pin  | MPU  |
| :---------------: | :--: | :--: |
|       3.3V        |  1   | VCC  |
|      Ground       |  14  | GND  |
| GPIO 9 (SCL1 i2c) |  5   | SCL  |
| GPIO 8 (SDA1 i2c) |  3   | SDA  |

Spécification pour mon Raspberry 2 B ( [source site Raspberry](https://www.raspberrypi.org/documentation/hardware/raspberrypi/schematics/rpi_SCH_2b_1p2_reduced.pdf) )

![1566118220242](assets/1566118220242.png)

Démarré SSH et NVC sur le Raspberry via <code>sudo raspi-config</code> menu *interfaces*. 

Connexions faites, on allume, rien n'explose. :thumbsup: 

Désolé pour la poussière, il est vieux Mr Raspberry...

![1566120854186](assets/1566120854186.png)

![1566120970597](assets/1566120970597.png)

Admirons ces magnifiques soudures, merci Rémy !

Accès *ssh* depuis mon PC ok, on essaye?... 

<code>sudo i2cdetect -y 1</code>

Youpi, il semble que le gyroscope est détecté. 68!... :happy:

![1566120418331](assets/1566120418331.png)

Il s'agit désormais de récupérer les données.

### Lire les données

Le composant a l'adresse 0x68 sur le bus ***i2c***, ensuite chaque data a son adresse. Les données intéressantes sont aux adresses suivantes :

![1566147444446](assets/1566147444446.png)

Commande ***i2cget*** pour lire ces données. Mettons qu'on souhaite lire 2 octets à l'adresse 0x43 ( GYRO_X_OUT) : <code>ic2get -y 1 0x68 0x43 w</code>

<code>-y </code> pour ne pas avoir a confirmer suite aux différents warnings

<code>1</code> : numéro du bus

<code>0x68</code> adresse du composant

<code>0x43</code> adresse de lecture 

<code>w</code>  pour word = 2 bytes = 2 octets = 16 bits

### Automatiser

Un moyen simple de lire ces données : python. J'ai adapté [un script pour un MPU-6000](https://github.com/ControlEverythingCommunity/MPU-6000/blob/master/Python/MPU_6000.py) pour lire les données. Je lis GYRO_XOUT, GYRO_YOUT et GYRO_ZOUT, ainsi que la température. 

![1566199788175](assets/1566199788175.png)

Sortie : 

![1566199129990](assets/1566199129990.png)

Tout cela est satisfaisant d'un point de vue de noob, tout semble fonctionner. Au niveau des résultats il  reste des choses à creuser :

* les données gyroscopiques ne me semblent pas bonnes : valeurs très basses quand je ne bouge pas le composant, quelle que soit sa position, et valeurs très élevées quand je suis en train de bouger le composant. Cela ressemble plus à des données de l'accéléromètre que du gyroscope. J'ai pourtant vérifié plusieurs fois la spec...
* il faut calibrer le gyroscope



Trouvé une librairie Python pour le MP9250 : [FaBo9Axis](https://github.com/FaBoPlatform/FaBo9AXIS-MPU9250-Python)

Installée via :

 <code>pip install FaBo9Axis_MPU9250</code>

Pour vérifier les librairies installée on peut utiliser :

<code>pip list</code>

Finalement impossible de m'en sortir avec les librairies, je décide de reprendre le [code de la librairie](https://github.com/FaBoPlatform/FaBo9AXIS-MPU9250-Python/blob/master/FaBo9Axis_MPU9250/MPU9250.py) + le code applicatif dans un seul et même fichier. Moche mais ça marche.

### Ajout d'un composant sur le bus I2C

![1566820307036](assets/1566820307036.png)

Bernard m'a prêté un autre composant : le **BME280** (temp, pression, altimètre). J'ai eu du mal à voir les 2 connectés. Ce n'est pas faute d'avoir vérifié les branchements, assez sûr de mon coup, mais rien.

Au final le Raspberry ne fonctionnait plus. Plus de réseau, plus de démarrage. Je démonte tout. Je rebranche le MPU seul, ça marche. Je branche l'autre seul, ça marche. Je rebranche le tout en série... ça marche. Va comprendre Charles. 

J'ai donc le MPU9250 en 0x68 et le BME280 en 0x76

![1566754013202](assets/1566754013202.png)

![1566754280920](assets/1566754280920.png)



Mauvaise nouvelle, en fouillant les internets je tombe sur [un commentaire](https://arduino.stackovernet.com/fr/q/10691#37171) qui suggère que nous ne sommes pas en possession d'un chip BME280 mais d'un BMP260. Id lu à 0xD0 = 0x56 et non 0x60. Pas vérifié pour l'instant mais on le garde à l'esprit.

J'essaye de déchiffrer cette [lib c++](https://github.com/sparkfun/SparkFun_BME280_Arduino_Library/) pour Arduino.

Lecture de 3 bytes en *BME280_TEMPERATURE_MSB_REG* 

```
#define BME280_TEMPERATURE_MSB_REG		0xFA //Temperature MSB
#define BME280_TEMPERATURE_LSB_REG		0xFB //Temperature LSB
#define BME280_TEMPERATURE_XLSB_REG		0xFC //Temperature XLSB
```

![1566827724354](assets/1566827724354.png)

```
#define BME280_DIG_T1_LSB_REG			0x88
#define BME280_DIG_T1_MSB_REG			0x89
#define BME280_DIG_T2_LSB_REG			0x8A
#define BME280_DIG_T2_MSB_REG			0x8B
#define BME280_DIG_T3_LSB_REG			0x8C
#define BME280_DIG_T3_MSB_REG			0x8D
```

Les données seraient donc là. 

![1566827844409](assets/1566827844409.png)

Le problème c'est que rien ne bouge. Toujours les mêmes data. Je pense à la lecture de la doc qu'il faut changer le mode. Le composant dort sûrement (mode SLEEP), vais essayer d'utiliser le mode FORCE : on force une mise à jour des registres, puis on les lit. Il y un 3eme mode NORMAL qui met à jour en permanence mais ça consomme plus et nous on est trop du genre à sauver la planète à la moindre occaze.

En repartant sur le script du MPU, je démarre doucement : test avec lecture de l'ID du composant, on devrait récupérer 0x58 (88 en décimal)

![1566837192223](assets/1566837192223.png)

<code> > I am 88</code>

Ça marche. Oui bon, on va pas tirer un :fireworks:

*[Vim](https://fr.wikipedia.org/wiki/Vim)* c'est sympa, et puis ça fait pro, mais comme je ne suis pas un pro je perds beaucoup de temps. Faut que je puisse utiliser mes outils, je vais monter un disque sur mon PC pointant sur le répertoire de mes scripts sur le Raspberry ( ~/Documents/cod = /home/pi/Documents/cod). On installe *sshfs* et on monte le disque :

<code>sudo apt install sshfs</code>

<code>sshfs pi@192.168.0.29:/home/pi/Documents/cod ~/Documents/myRasp</code>

Et voilà !

![1566837972131](assets/1566837972131.png)

On y accède désormais comme un répertoire local.

![1566838141253](assets/1566838141253.png)

Je peux dès lors utiliser mes outils locaux, notamment mon éditeur de code.

![1566838545737](assets/1566838545737.png)

Ouf.

*Note* : j'ai eu besoin de transférer un script, j'apprends la commande ***scp***, quelle merveille Linux :) ([source](https://unix.stackexchange.com/questions/106480/how-to-copy-files-from-one-machine-to-another-using-ssh))

```
scp /path/to/file username@a:/path/to/destination
```



Je coupe court après quelques investigations supplémentaires car je n'ai plus le composant en ma possession. 

J'ai pu avancer un peu dans la lecture et l'écriture de registres afin de faire un *reset* ou changer le mode du composant, mais pas pu aller jusqu'à une exploitation des résultats. Mes quelques lignes de python : 

```python
import smbus
import time

## bus declaration
bus = smbus.SMBus(1)

# spec : https://embeddedadventures.com/datasheets/BME280.pdf
COMP_ADDR = 0x76
DATA_RESET_BYTE = 0xB6 # to be written at REG_RESET_ADDR to reset

REG_WHOAMI_ADDR = 0xD0
REG_RESET_ADDR = 0xE0
REG_MEAS_ADDR = 0xF4 # includes mode bits
## BME280 

who_am_i = bus.read_byte_data(COMP_ADDR, REG_WHOAMI_ADDR)
print( "I am " , who_am_i )

# reset component 
bus.write_byte_data(COMP_ADDR, REG_RESET_ADDR, DATA_RESET_BYTE)
print("Reset done.")

#get mode 
data = bus.read_i2c_block_data(COMP_ADDR, REG_MEAS_ADDR)
print ("Mode : " , data[0])

# set mode, 2 lowest bits (00 = sleep, 10 or 01 = forced, 11 = normal)
mode = (data[0] & 0xFC) | 0x02 # old mode , clearing 2 mode beats, adding new mode bits
bus.write_byte_data(COMP_ADDR,REG_MEAS_ADDR, mode)
print ("mode set to ", mode)
#get mode to check
data = bus.read_i2c_block_data(COMP_ADDR, REG_MEAS_ADDR)
print( "Mode read after modification: " , data[0] )

```



## Bilan

Bilan mitigé. 

C'est toujours satisfaisant d'apprendre des choses et d'avoir des petites victoires. Je pratique Linux, au final le gyroscope semble fonctionner, mais pas de résultats probants pour l'heure côté BME. 

C'était sympa de faire un peu de bidouille mais si j'y reviens j'essaierai probablement d'utiliser des bibliothèques ou logiciels qui s'occupent du "bas niveau", ce qui, je le précise pour nos amis électroniciens, n'est évidemment pas un jugement de valeur :wink:

## Notes

Commande pour lecture globale des registres :

<code>sudo i2cdump -y 1 ADRESSE_COMPOSANT</code>

i2ctools man pages https://manpages.debian.org/testing/i2c-tools/index.html

