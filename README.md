Notes personnelles d'un noob qui essaye d'exploiter son vieux Raspberry en se mettant un peu à la micro électronique (connexion de capteurs).

Ces notes sont publiées en version web pour partage, les fichiers html sont générés par Typora (Export HTML) qui sert d'outil de prises de notes.

Ce dépot contient les fichiers de cette version web, et les sources en markdown. Les fichiers .md sont au même niveau que les .html exportés pour simplifier les liens vers les */assets*, ce qui n'est pas très malin. On fera mieux la prochaine fois. 
