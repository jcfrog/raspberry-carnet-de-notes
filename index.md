<div style="text-align:right;color:grey">Dernière mise à jour : 1er septembre 2019</div>

# Carnet de notes Raspberry

Dans le cadre des activités de l'Université Populaire du Numérique de Damgan nous avons créé un groupe Raspberry. Voir [cette page de notre wiki](https://adn56.net/wiki/index.php/Activit%C3%A9_Raspberry) pour plus de détails.

Je suis les savants enseignements de l'ami Bernard mais je procède également à des manips à la maison avec mon vieux Raspberry 2.

Ce carnet de notes personnelles sert à journaliser mes différentes manipulations et partager avec qui sera intéressé mes avancées en la matière.

J'en profite pour apprendre à me servir de [Typora](https://typora.io/) avec lequel sont mises en pages ces notes, et le plus grand nombre possible de logiciels libres pour changer mes habitudes dans mon nouvel environnement (me voilà Ubunté).

Feuillets :

* [installation d'un serveur Web/PHP/MySQL](server.html)
* [prise en main et connexion d'un composant gyroscope](gyro.html)
* [connecter et allumer une LED par commande logiciel (python)](led.html)
* [piloter mes scripts python depuis une page web](webcontrol.html)

